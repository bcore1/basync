local waiting = []

local function packet_call(parent) {
    waiting.push({ parent = parent.weakref(), header = this.header, pid = this.pid})
    return {result = null, status = "suspended"}
}

if (CLIENT_SIDE) {
    function basync::packet(header) {
        return { call = packet_call, header = header, pid = null }
    }
} else {
    function basync::packet(pid, header) {
        return { call = packet_call, header = header, pid = pid }
    }
}

local function parent_wakeup(pid, packet) {
    local header = packet.readUInt8()
    local remove = []

    foreach (idx, entry in waiting) {
        if (header == entry.header && pid == entry.pid) {
            if (entry.parent) {
                entry.parent.wakeup(packet)
            }

            remove.push(idx)
        }
    }

    for (local i = remove.len() - 1; i >= 0; --i) {
        waiting.remove(remove[i])
    }
}

local function cli_packet_handler(packet) {
    parent_wakeup(null, packet)
}

local function srv_packet_handler(pid, packet) {
    parent_wakeup(pid, packet)
}

if (CLIENT_SIDE) {
    addEventHandler("onPacket", cli_packet_handler)
} else {
    addEventHandler("onPacket", srv_packet_handler)
}