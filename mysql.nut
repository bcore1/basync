local function mysql_connect_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_connect_async(this.host, this.username, this.password, this.database, this.port, this.socket, function(conn) {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(conn)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_connect(host, username, password, database, port = null, socket = null) {
	return { call = mysql_connect_call, host = host, username = username, password = password, database = database, port = port, socket = socket }
}

local function mysql_close_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_close_async(this.conn, function() {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(conn)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_close(conn) {
	return { call = mysql_close_call, conn = conn }
}

local function mysql_select_db_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_select_db_async(this.conn, function(success) {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(success)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_select_db(conn) {
	return { call = mysql_close_call, conn = conn }
}

local function mysql_query_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_query_async(this.conn, this.query, function(result) {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(result)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_query(conn, query) {
	return { call = mysql_query_call, conn = conn, query = query }
}

local function mysql_ping_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_ping_async(this.conn, function(status) {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(status)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_ping(conn) {
	return { call = mysql_ping_call, conn = conn }
}

local function mysql_set_character_set_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_set_character_set_async(this.conn, this.charset, function(success) {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(success)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_set_character_set(conn, charset) {
	return { call = mysql_set_character_set_call, conn = conn, charset = charset }
}

local function mysql_real_escape_string_call(parent) {
    local parent_ptr = parent.weakref()

	mysql_real_escape_string_async(this.conn, this.text, function(escaped_text) {
		local parent = parent_ptr.ref()

		if (parent)
			parent.wakeup(escaped_text)
	})

    return {result = null, status = "suspended"}
}

function basync::mysql_real_escape_string(conn, text) {
	return { call = mysql_real_escape_string_call, conn = conn, text = text }
}