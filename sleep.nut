local function sleep_timer(parent_ptr) {
    local parent = parent_ptr.ref()
    if (parent) {
        parent.wakeup(null)
    }
}

local function sleep_call(parent) {
    setTimer(sleep_timer, this.interval, 1, parent.weakref())
    return {result = null, status = "suspended"}
}

function basync::sleep(ms) {
    return { call = sleep_call, interval = ms }
}
