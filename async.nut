basync <- {}

local function parent_wakeup(parent_ptr, result) {
    local parent = parent_ptr.ref()
    if (parent && parent.getstatus() == "suspended") {
        return parent.wakeup(result)
    }
    
    return result
}

// .acall won't work in coroutine context
local function awaitable0(ctx, parent, callable) {
    return parent_wakeup(parent, callable(ctx))
}

local function awaitable1(ctx, parent, callable, a) {
    return parent_wakeup(parent, callable(ctx, a))
}

local function awaitable2(ctx, parent, callable, a, b) {
    return parent_wakeup(parent, callable(ctx, a, b))
}

local function awaitable3(ctx, parent, callable, a, b, c) {
    return parent_wakeup(parent, callable(ctx, a, b, c))
}

local function awaitable4(ctx, parent, callable, a, b, c, d) {
    return parent_wakeup(parent, callable(ctx, a, b, c, d))
}

local function awaitable5(ctx, parent, callable, a, b, c, d, e) {
    return parent_wakeup(parent, callable(ctx, a, b, c, d, e))
}

local function awaitable6(ctx, parent, callable, a, b, c, d, e, f) {
    return parent_wakeup(parent, callable(ctx, a, b, c, d, e, f))
}

local function awaitable7(ctx, parent, callable, a, b, c, d, e, f, g) {
    return parent_wakeup(parent, callable(ctx, a, b, c, d, e, f, g))
}

local function awaitable8(ctx, parent, callable, a, b, c, d, e, f, g, h) {
    return parent_wakeup(parent, callable(ctx, a, b, c, d, e, f, g, h))
}

local awaitables = [
    awaitable0,
    awaitable1,
    awaitable2,
    awaitable3,
    awaitable4,
    awaitable5,
    awaitable6,
    awaitable7,
    awaitable8
]

local awaitable_wrapper = {
    function _call(root, ...) {
        this.args = vargv
        return this
    }

    function call(parent) {
        local coro = ::newthread(awaitables[this.args.len()])

        local args = [coro, coro, parent.weakref(), this.callable]
        args.extend(this.args)

        return {result = coro.call.acall(args), status = coro.getstatus()}
    }
}

local task_wrapper = {
    function _call(root, callable) {
        local awaitable = {callable = callable, args = null}
        awaitable.setdelegate(awaitable_wrapper)

        return awaitable
    }
}

basync.setdelegate(task_wrapper)

function basync::await(parent, awaitable) {
    local r = awaitable.call(parent)
    if (r.status == "idle") {
        return r.result
    }

    return ::suspend(r.result)
}

function basync::run(awaitable_or_closure, params = null) {
    local callable = awaitable_or_closure
    if (type(awaitable_or_closure) != "function") {
        callable = awaitable_or_closure.callable
        params = awaitable_or_closure.args
    }

    local coro = ::newthread(callable)

    local args = [coro, coro]
    if (params) {
        args.extend(params)
    }
    
    local result = coro.call.acall(args)
    return {task = coro, result = result}
}