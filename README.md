# BAsync - Library to write concurrent code using the async/await syntax for Gothic 2 Online

```js
// Closure to async function
basync.run(function (ctx) {
    print("A")
    basync.await(ctx, basync.sleep(2500))
    print("B")
})

// Closure to async function with parameters
basync.run(function (ctx, a, b) {
    print("A: " + a)
    basync.await(ctx, basync.sleep(2500))
    print("B: " + b)
}, [1, 2])


// Deep nested async functions
local function f3(ctx) {
    print("THREAD: 3")
    basync.await(ctx, basync.sleep(1000))
    print("THREAD: 3")
}

local f2 = basync(function (ctx, a, b) {
    print("THREAD: 2")

    // Only function marked as async can be awaited!
    basync.await(ctx, basync(f3)())

    print("THREAD: 2")
    printf("RESULT: %d", a + b)
})

local f1 = basync(function (ctx) {
    print("THREAD: 1")
    basync.await(ctx, basync.sleep(1000))
    print("THREAD: 1")
    basync.await(ctx, f2(1, 2))
    print("THREAD: 1")
    print("FINISHED")
})

local r = basync.run(f1())
print(r.task.getstatus())
```

## More practical example

**Shared:**

```js
enum PacketHeader {
    REQUEST,
    RESPONSE
}
```

**Server:**

```js
local function packet_handler(pid, packet) {
    // Client requested some data
    if (packet.readUInt8() == PacketHeader.REQUEST) {
        // Simulate lag on server
        setTimer(function () {
            local response = Packet()
            response.writeUInt8(PacketHeader.RESPONSE)
            response.writeString("Hello async!")

            response.send(pid, RELIABLE)
        }, 2000, 1)
    }
}

addEventHandler("onPacket", packet_handler)
```

**Client:**

```js
local loading_label = Draw(3500, 3500, "Waiting for data...")
local message_label = Draw(3500, 4500, "")

local function init_handler() {
    message_label.setColor(255, 255, 0)

    local request = Packet()
    request.writeInt8(PacketHeader.REQUEST)
    request.send(RELIABLE_ORDERED)

    basync.run(function(ctx) {
        loading_label.visible = true
        local packet = basync.await(ctx, basync.packet(PacketHeader.RESPONSE))
        loading_label.visible = false

        message_label.text = packet.readString()
        message_label.visible = true
    })
}

addEventHandler("onInit", init_handler)
```
